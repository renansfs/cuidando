PGDMP     5                    u            esic    9.5.6    9.5.6     	           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            	           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            	           1262    16385    esic    DATABASE     v   CREATE DATABASE esic WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE esic;
             renan    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            	           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    6            	           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    6                        3079    12395    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            	           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    24796 
   attachment    TABLE     �   CREATE TABLE attachment (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone,
    ia_url text
);
    DROP TABLE public.attachment;
       public         renan    false    6            �            1259    24794    attachment_id_seq    SEQUENCE     s   CREATE SEQUENCE attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.attachment_id_seq;
       public       renan    false    6    184            	           0    0    attachment_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE attachment_id_seq OWNED BY attachment.id;
            public       renan    false    183            �            1259    24860    attachment_recurso    TABLE     �   CREATE TABLE attachment_recurso (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone,
    ia_url text
);
 &   DROP TABLE public.attachment_recurso;
       public         renan    false    6            �            1259    24858    attachment_recurso_id_seq    SEQUENCE     {   CREATE SEQUENCE attachment_recurso_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.attachment_recurso_id_seq;
       public       renan    false    196    6            	           0    0    attachment_recurso_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE attachment_recurso_id_seq OWNED BY attachment_recurso.id;
            public       renan    false    195            �            1259    24817    author    TABLE     [   CREATE TABLE author (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);
    DROP TABLE public.author;
       public         renan    false    6            �            1259    24815    author_id_seq    SEQUENCE     o   CREATE SEQUENCE author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.author_id_seq;
       public       renan    false    6    188            	           0    0    author_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE author_id_seq OWNED BY author.id;
            public       renan    false    187            �            1259    24851    keyword    TABLE     \   CREATE TABLE keyword (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);
    DROP TABLE public.keyword;
       public         renan    false    6            �            1259    24849    keyword_id_seq    SEQUENCE     p   CREATE SEQUENCE keyword_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.keyword_id_seq;
       public       renan    false    6    194            	           0    0    keyword_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE keyword_id_seq OWNED BY keyword.id;
            public       renan    false    193            �            1259    24965    message    TABLE     �   CREATE TABLE message (
    id integer NOT NULL,
    situation character varying(255),
    justification text,
    responsible character varying(255),
    date timestamp without time zone,
    pedido_id integer,
    id_recurso integer
);
    DROP TABLE public.message;
       public         renan    false    6            �            1259    24963    message_id_seq    SEQUENCE     p   CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.message_id_seq;
       public       renan    false    208    6            	           0    0    message_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE message_id_seq OWNED BY message.id;
            public       renan    false    207            �            1259    24807    orgao    TABLE     Z   CREATE TABLE orgao (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);
    DROP TABLE public.orgao;
       public         renan    false    6            �            1259    24805    orgao_id_seq    SEQUENCE     n   CREATE SEQUENCE orgao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.orgao_id_seq;
       public       renan    false    186    6            	           0    0    orgao_id_seq    SEQUENCE OWNED BY     /   ALTER SEQUENCE orgao_id_seq OWNED BY orgao.id;
            public       renan    false    185            �            1259    24787    orgaos_update    TABLE     ^   CREATE TABLE orgaos_update (
    id integer NOT NULL,
    date timestamp without time zone
);
 !   DROP TABLE public.orgaos_update;
       public         renan    false    6            �            1259    24785    orgaos_update_id_seq    SEQUENCE     v   CREATE SEQUENCE orgaos_update_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.orgaos_update_id_seq;
       public       renan    false    6    182            	           0    0    orgaos_update_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE orgaos_update_id_seq OWNED BY orgaos_update.id;
            public       renan    false    181            �            1259    24827    pedido    TABLE     U  CREATE TABLE pedido (
    id integer NOT NULL,
    protocol integer,
    interessado character varying(255),
    situation character varying(255),
    request_date timestamp without time zone,
    contact_option character varying(255),
    description text,
    deadline timestamp without time zone,
    orgao_name character varying(255)
);
    DROP TABLE public.pedido;
       public         renan    false    6            �            1259    24902    pedido_attachments    TABLE     V   CREATE TABLE pedido_attachments (
    pedido_id integer,
    attachment_id integer
);
 &   DROP TABLE public.pedido_attachments;
       public         renan    false    6            �            1259    24937    pedido_author    TABLE     M   CREATE TABLE pedido_author (
    pedido_id integer,
    author_id integer
);
 !   DROP TABLE public.pedido_author;
       public         renan    false    6            �            1259    24825    pedido_id_seq    SEQUENCE     o   CREATE SEQUENCE pedido_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.pedido_id_seq;
       public       renan    false    6    190            	           0    0    pedido_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE pedido_id_seq OWNED BY pedido.id;
            public       renan    false    189            �            1259    24889    pedido_keyword    TABLE     O   CREATE TABLE pedido_keyword (
    pedido_id integer,
    keyword_id integer
);
 "   DROP TABLE public.pedido_keyword;
       public         renan    false    6            �            1259    24842    pedidos_update    TABLE     _   CREATE TABLE pedidos_update (
    id integer NOT NULL,
    date timestamp without time zone
);
 "   DROP TABLE public.pedidos_update;
       public         renan    false    6            �            1259    24840    pedidos_update_id_seq    SEQUENCE     w   CREATE SEQUENCE pedidos_update_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.pedidos_update_id_seq;
       public       renan    false    6    192            	           0    0    pedidos_update_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE pedidos_update_id_seq OWNED BY pedidos_update.id;
            public       renan    false    191            �            1259    24880 
   pre_pedido    TABLE     3  CREATE TABLE pre_pedido (
    id integer NOT NULL,
    author_id integer,
    orgao_name character varying(255),
    text text,
    keywords character varying(255),
    state character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    tipo integer
);
    DROP TABLE public.pre_pedido;
       public         renan    false    6            �            1259    24878    pre_pedido_id_seq    SEQUENCE     s   CREATE SEQUENCE pre_pedido_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.pre_pedido_id_seq;
       public       renan    false    200    6            	           0    0    pre_pedido_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE pre_pedido_id_seq OWNED BY pre_pedido.id;
            public       renan    false    199            �            1259    24917    recurso    TABLE     #  CREATE TABLE recurso (
    id integer NOT NULL,
    pedido_id integer NOT NULL,
    protocol integer,
    situation character varying(255),
    request_date timestamp without time zone,
    description text,
    deadline timestamp without time zone,
    orgao_name character varying(255)
);
    DROP TABLE public.recurso;
       public         renan    false    6            �            1259    24950    recurso_attachments    TABLE     `   CREATE TABLE recurso_attachments (
    recurso_id integer,
    attachment_recurso_id integer
);
 '   DROP TABLE public.recurso_attachments;
       public         renan    false    6            �            1259    24915    recurso_id_seq    SEQUENCE     p   CREATE SEQUENCE recurso_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.recurso_id_seq;
       public       renan    false    204    6            	           0    0    recurso_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE recurso_id_seq OWNED BY recurso.id;
            public       renan    false    203            �            1259    24871    recursos_update    TABLE     `   CREATE TABLE recursos_update (
    id integer NOT NULL,
    date timestamp without time zone
);
 #   DROP TABLE public.recursos_update;
       public         renan    false    6            �            1259    24869    recursos_update_id_seq    SEQUENCE     x   CREATE SEQUENCE recursos_update_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.recursos_update_id_seq;
       public       renan    false    198    6            	           0    0    recursos_update_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE recursos_update_id_seq OWNED BY recursos_update.id;
            public       renan    false    197            <           2604    24799    id    DEFAULT     `   ALTER TABLE ONLY attachment ALTER COLUMN id SET DEFAULT nextval('attachment_id_seq'::regclass);
 <   ALTER TABLE public.attachment ALTER COLUMN id DROP DEFAULT;
       public       renan    false    184    183    184            B           2604    24863    id    DEFAULT     p   ALTER TABLE ONLY attachment_recurso ALTER COLUMN id SET DEFAULT nextval('attachment_recurso_id_seq'::regclass);
 D   ALTER TABLE public.attachment_recurso ALTER COLUMN id DROP DEFAULT;
       public       renan    false    196    195    196            >           2604    24820    id    DEFAULT     X   ALTER TABLE ONLY author ALTER COLUMN id SET DEFAULT nextval('author_id_seq'::regclass);
 8   ALTER TABLE public.author ALTER COLUMN id DROP DEFAULT;
       public       renan    false    188    187    188            A           2604    24854    id    DEFAULT     Z   ALTER TABLE ONLY keyword ALTER COLUMN id SET DEFAULT nextval('keyword_id_seq'::regclass);
 9   ALTER TABLE public.keyword ALTER COLUMN id DROP DEFAULT;
       public       renan    false    194    193    194            F           2604    24968    id    DEFAULT     Z   ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);
 9   ALTER TABLE public.message ALTER COLUMN id DROP DEFAULT;
       public       renan    false    208    207    208            =           2604    24810    id    DEFAULT     V   ALTER TABLE ONLY orgao ALTER COLUMN id SET DEFAULT nextval('orgao_id_seq'::regclass);
 7   ALTER TABLE public.orgao ALTER COLUMN id DROP DEFAULT;
       public       renan    false    186    185    186            ;           2604    24790    id    DEFAULT     f   ALTER TABLE ONLY orgaos_update ALTER COLUMN id SET DEFAULT nextval('orgaos_update_id_seq'::regclass);
 ?   ALTER TABLE public.orgaos_update ALTER COLUMN id DROP DEFAULT;
       public       renan    false    181    182    182            ?           2604    24830    id    DEFAULT     X   ALTER TABLE ONLY pedido ALTER COLUMN id SET DEFAULT nextval('pedido_id_seq'::regclass);
 8   ALTER TABLE public.pedido ALTER COLUMN id DROP DEFAULT;
       public       renan    false    190    189    190            @           2604    24845    id    DEFAULT     h   ALTER TABLE ONLY pedidos_update ALTER COLUMN id SET DEFAULT nextval('pedidos_update_id_seq'::regclass);
 @   ALTER TABLE public.pedidos_update ALTER COLUMN id DROP DEFAULT;
       public       renan    false    192    191    192            D           2604    24883    id    DEFAULT     `   ALTER TABLE ONLY pre_pedido ALTER COLUMN id SET DEFAULT nextval('pre_pedido_id_seq'::regclass);
 <   ALTER TABLE public.pre_pedido ALTER COLUMN id DROP DEFAULT;
       public       renan    false    199    200    200            E           2604    24920    id    DEFAULT     Z   ALTER TABLE ONLY recurso ALTER COLUMN id SET DEFAULT nextval('recurso_id_seq'::regclass);
 9   ALTER TABLE public.recurso ALTER COLUMN id DROP DEFAULT;
       public       renan    false    204    203    204            C           2604    24874    id    DEFAULT     j   ALTER TABLE ONLY recursos_update ALTER COLUMN id SET DEFAULT nextval('recursos_update_id_seq'::regclass);
 A   ALTER TABLE public.recursos_update ALTER COLUMN id DROP DEFAULT;
       public       renan    false    197    198    198            �          0    24796 
   attachment 
   TABLE DATA               ;   COPY attachment (id, name, created_at, ia_url) FROM stdin;
    public       renan    false    184   R�       	           0    0    attachment_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('attachment_id_seq', 1, false);
            public       renan    false    183            �          0    24860    attachment_recurso 
   TABLE DATA               C   COPY attachment_recurso (id, name, created_at, ia_url) FROM stdin;
    public       renan    false    196   o�        	           0    0    attachment_recurso_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('attachment_recurso_id_seq', 1, false);
            public       renan    false    195            �          0    24817    author 
   TABLE DATA               #   COPY author (id, name) FROM stdin;
    public       renan    false    188   ��       !	           0    0    author_id_seq    SEQUENCE SET     4   SELECT pg_catalog.setval('author_id_seq', 2, true);
            public       renan    false    187            �          0    24851    keyword 
   TABLE DATA               $   COPY keyword (id, name) FROM stdin;
    public       renan    false    194   ��       "	           0    0    keyword_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('keyword_id_seq', 4, true);
            public       renan    false    193            
	          0    24965    message 
   TABLE DATA               b   COPY message (id, situation, justification, responsible, date, pedido_id, id_recurso) FROM stdin;
    public       renan    false    208   �       #	           0    0    message_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('message_id_seq', 12, true);
            public       renan    false    207            �          0    24807    orgao 
   TABLE DATA               "   COPY orgao (id, name) FROM stdin;
    public       renan    false    186   ��       $	           0    0    orgao_id_seq    SEQUENCE SET     4   SELECT pg_catalog.setval('orgao_id_seq', 78, true);
            public       renan    false    185            �          0    24787    orgaos_update 
   TABLE DATA               *   COPY orgaos_update (id, date) FROM stdin;
    public       renan    false    182    �       %	           0    0    orgaos_update_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('orgaos_update_id_seq', 1, true);
            public       renan    false    181            �          0    24827    pedido 
   TABLE DATA               �   COPY pedido (id, protocol, interessado, situation, request_date, contact_option, description, deadline, orgao_name) FROM stdin;
    public       renan    false    190   :�       	          0    24902    pedido_attachments 
   TABLE DATA               ?   COPY pedido_attachments (pedido_id, attachment_id) FROM stdin;
    public       renan    false    202   А       	          0    24937    pedido_author 
   TABLE DATA               6   COPY pedido_author (pedido_id, author_id) FROM stdin;
    public       renan    false    205   �       &	           0    0    pedido_id_seq    SEQUENCE SET     4   SELECT pg_catalog.setval('pedido_id_seq', 4, true);
            public       renan    false    189            	          0    24889    pedido_keyword 
   TABLE DATA               8   COPY pedido_keyword (pedido_id, keyword_id) FROM stdin;
    public       renan    false    201   �       �          0    24842    pedidos_update 
   TABLE DATA               +   COPY pedidos_update (id, date) FROM stdin;
    public       renan    false    192   A�       '	           0    0    pedidos_update_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('pedidos_update_id_seq', 1, false);
            public       renan    false    191            	          0    24880 
   pre_pedido 
   TABLE DATA               m   COPY pre_pedido (id, author_id, orgao_name, text, keywords, state, created_at, updated_at, tipo) FROM stdin;
    public       renan    false    200   ^�       (	           0    0    pre_pedido_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('pre_pedido_id_seq', 5, true);
            public       renan    false    199            	          0    24917    recurso 
   TABLE DATA               o   COPY recurso (id, pedido_id, protocol, situation, request_date, description, deadline, orgao_name) FROM stdin;
    public       renan    false    204   ��       	          0    24950    recurso_attachments 
   TABLE DATA               I   COPY recurso_attachments (recurso_id, attachment_recurso_id) FROM stdin;
    public       renan    false    206   ��       )	           0    0    recurso_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('recurso_id_seq', 1, true);
            public       renan    false    203             	          0    24871    recursos_update 
   TABLE DATA               ,   COPY recursos_update (id, date) FROM stdin;
    public       renan    false    198   ��       *	           0    0    recursos_update_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('recursos_update_id_seq', 1, false);
            public       renan    false    197            K           2606    24804    attachment_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY attachment
    ADD CONSTRAINT attachment_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.attachment DROP CONSTRAINT attachment_pkey;
       public         renan    false    184    184            a           2606    24868    attachment_recurso_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY attachment_recurso
    ADD CONSTRAINT attachment_recurso_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.attachment_recurso DROP CONSTRAINT attachment_recurso_pkey;
       public         renan    false    196    196            Q           2606    24824    author_name_key 
   CONSTRAINT     J   ALTER TABLE ONLY author
    ADD CONSTRAINT author_name_key UNIQUE (name);
 @   ALTER TABLE ONLY public.author DROP CONSTRAINT author_name_key;
       public         renan    false    188    188            S           2606    24822    author_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.author DROP CONSTRAINT author_pkey;
       public         renan    false    188    188            _           2606    24856    keyword_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY keyword
    ADD CONSTRAINT keyword_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.keyword DROP CONSTRAINT keyword_pkey;
       public         renan    false    194    194            q           2606    24973    message_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.message DROP CONSTRAINT message_pkey;
       public         renan    false    208    208            M           2606    24814    orgao_name_key 
   CONSTRAINT     H   ALTER TABLE ONLY orgao
    ADD CONSTRAINT orgao_name_key UNIQUE (name);
 >   ALTER TABLE ONLY public.orgao DROP CONSTRAINT orgao_name_key;
       public         renan    false    186    186            O           2606    24812 
   orgao_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY orgao
    ADD CONSTRAINT orgao_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.orgao DROP CONSTRAINT orgao_pkey;
       public         renan    false    186    186            I           2606    24792    orgaos_update_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY orgaos_update
    ADD CONSTRAINT orgaos_update_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.orgaos_update DROP CONSTRAINT orgaos_update_pkey;
       public         renan    false    182    182            Y           2606    24835    pedido_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY pedido
    ADD CONSTRAINT pedido_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.pedido DROP CONSTRAINT pedido_pkey;
       public         renan    false    190    190            \           2606    24847    pedidos_update_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY pedidos_update
    ADD CONSTRAINT pedidos_update_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.pedidos_update DROP CONSTRAINT pedidos_update_pkey;
       public         renan    false    192    192            f           2606    24888    pre_pedido_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY pre_pedido
    ADD CONSTRAINT pre_pedido_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.pre_pedido DROP CONSTRAINT pre_pedido_pkey;
       public         renan    false    200    200            l           2606    24927    recurso_id_key 
   CONSTRAINT     H   ALTER TABLE ONLY recurso
    ADD CONSTRAINT recurso_id_key UNIQUE (id);
 @   ALTER TABLE ONLY public.recurso DROP CONSTRAINT recurso_id_key;
       public         renan    false    204    204            n           2606    24925    recurso_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY recurso
    ADD CONSTRAINT recurso_pkey PRIMARY KEY (id, pedido_id);
 >   ALTER TABLE ONLY public.recurso DROP CONSTRAINT recurso_pkey;
       public         renan    false    204    204    204            d           2606    24876    recursos_update_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY recursos_update
    ADD CONSTRAINT recursos_update_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.recursos_update DROP CONSTRAINT recursos_update_pkey;
       public         renan    false    198    198            ]           1259    24857    ix_keyword_name    INDEX     C   CREATE UNIQUE INDEX ix_keyword_name ON keyword USING btree (name);
 #   DROP INDEX public.ix_keyword_name;
       public         renan    false    194            o           1259    24984    ix_message_date    INDEX     <   CREATE INDEX ix_message_date ON message USING btree (date);
 #   DROP INDEX public.ix_message_date;
       public         renan    false    208            G           1259    24793    ix_orgaos_update_date    INDEX     H   CREATE INDEX ix_orgaos_update_date ON orgaos_update USING btree (date);
 )   DROP INDEX public.ix_orgaos_update_date;
       public         renan    false    182            T           1259    24838    ix_pedido_deadline    INDEX     B   CREATE INDEX ix_pedido_deadline ON pedido USING btree (deadline);
 &   DROP INDEX public.ix_pedido_deadline;
       public         renan    false    190            U           1259    24836    ix_pedido_protocol    INDEX     I   CREATE UNIQUE INDEX ix_pedido_protocol ON pedido USING btree (protocol);
 &   DROP INDEX public.ix_pedido_protocol;
       public         renan    false    190            V           1259    24837    ix_pedido_request_date    INDEX     J   CREATE INDEX ix_pedido_request_date ON pedido USING btree (request_date);
 *   DROP INDEX public.ix_pedido_request_date;
       public         renan    false    190            W           1259    24839    ix_pedido_situation    INDEX     D   CREATE INDEX ix_pedido_situation ON pedido USING btree (situation);
 '   DROP INDEX public.ix_pedido_situation;
       public         renan    false    190            Z           1259    24848    ix_pedidos_update_date    INDEX     J   CREATE INDEX ix_pedidos_update_date ON pedidos_update USING btree (date);
 *   DROP INDEX public.ix_pedidos_update_date;
       public         renan    false    192            g           1259    24933    ix_recurso_deadline    INDEX     D   CREATE INDEX ix_recurso_deadline ON recurso USING btree (deadline);
 '   DROP INDEX public.ix_recurso_deadline;
       public         renan    false    204            h           1259    24935    ix_recurso_protocol    INDEX     K   CREATE UNIQUE INDEX ix_recurso_protocol ON recurso USING btree (protocol);
 '   DROP INDEX public.ix_recurso_protocol;
       public         renan    false    204            i           1259    24934    ix_recurso_request_date    INDEX     L   CREATE INDEX ix_recurso_request_date ON recurso USING btree (request_date);
 +   DROP INDEX public.ix_recurso_request_date;
       public         renan    false    204            j           1259    24936    ix_recurso_situation    INDEX     F   CREATE INDEX ix_recurso_situation ON recurso USING btree (situation);
 (   DROP INDEX public.ix_recurso_situation;
       public         renan    false    204            b           1259    24877    ix_recursos_update_date    INDEX     L   CREATE INDEX ix_recursos_update_date ON recursos_update USING btree (date);
 +   DROP INDEX public.ix_recursos_update_date;
       public         renan    false    198            |           2606    24979    message_id_recurso_fkey    FK CONSTRAINT     u   ALTER TABLE ONLY message
    ADD CONSTRAINT message_id_recurso_fkey FOREIGN KEY (id_recurso) REFERENCES recurso(id);
 I   ALTER TABLE ONLY public.message DROP CONSTRAINT message_id_recurso_fkey;
       public       renan    false    2156    204    208            {           2606    24974    message_pedido_id_fkey    FK CONSTRAINT     r   ALTER TABLE ONLY message
    ADD CONSTRAINT message_pedido_id_fkey FOREIGN KEY (pedido_id) REFERENCES pedido(id);
 H   ALTER TABLE ONLY public.message DROP CONSTRAINT message_pedido_id_fkey;
       public       renan    false    190    2137    208            u           2606    24910 %   pedido_attachments_attachment_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY pedido_attachments
    ADD CONSTRAINT pedido_attachments_attachment_id_fkey FOREIGN KEY (attachment_id) REFERENCES attachment(id);
 b   ALTER TABLE ONLY public.pedido_attachments DROP CONSTRAINT pedido_attachments_attachment_id_fkey;
       public       renan    false    184    2123    202            t           2606    24905 !   pedido_attachments_pedido_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY pedido_attachments
    ADD CONSTRAINT pedido_attachments_pedido_id_fkey FOREIGN KEY (pedido_id) REFERENCES pedido(id);
 ^   ALTER TABLE ONLY public.pedido_attachments DROP CONSTRAINT pedido_attachments_pedido_id_fkey;
       public       renan    false    190    2137    202            x           2606    24945    pedido_author_author_id_fkey    FK CONSTRAINT     ~   ALTER TABLE ONLY pedido_author
    ADD CONSTRAINT pedido_author_author_id_fkey FOREIGN KEY (author_id) REFERENCES author(id);
 T   ALTER TABLE ONLY public.pedido_author DROP CONSTRAINT pedido_author_author_id_fkey;
       public       renan    false    188    205    2131            w           2606    24940    pedido_author_pedido_id_fkey    FK CONSTRAINT     ~   ALTER TABLE ONLY pedido_author
    ADD CONSTRAINT pedido_author_pedido_id_fkey FOREIGN KEY (pedido_id) REFERENCES pedido(id);
 T   ALTER TABLE ONLY public.pedido_author DROP CONSTRAINT pedido_author_pedido_id_fkey;
       public       renan    false    190    205    2137            s           2606    24897    pedido_keyword_keyword_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY pedido_keyword
    ADD CONSTRAINT pedido_keyword_keyword_id_fkey FOREIGN KEY (keyword_id) REFERENCES keyword(id);
 W   ALTER TABLE ONLY public.pedido_keyword DROP CONSTRAINT pedido_keyword_keyword_id_fkey;
       public       renan    false    201    194    2143            r           2606    24892    pedido_keyword_pedido_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY pedido_keyword
    ADD CONSTRAINT pedido_keyword_pedido_id_fkey FOREIGN KEY (pedido_id) REFERENCES pedido(id);
 V   ALTER TABLE ONLY public.pedido_keyword DROP CONSTRAINT pedido_keyword_pedido_id_fkey;
       public       renan    false    201    2137    190            z           2606    24958 .   recurso_attachments_attachment_recurso_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY recurso_attachments
    ADD CONSTRAINT recurso_attachments_attachment_recurso_id_fkey FOREIGN KEY (attachment_recurso_id) REFERENCES attachment_recurso(id);
 l   ALTER TABLE ONLY public.recurso_attachments DROP CONSTRAINT recurso_attachments_attachment_recurso_id_fkey;
       public       renan    false    196    2145    206            y           2606    24953 #   recurso_attachments_recurso_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY recurso_attachments
    ADD CONSTRAINT recurso_attachments_recurso_id_fkey FOREIGN KEY (recurso_id) REFERENCES recurso(id);
 a   ALTER TABLE ONLY public.recurso_attachments DROP CONSTRAINT recurso_attachments_recurso_id_fkey;
       public       renan    false    206    204    2156            v           2606    24928    recurso_pedido_id_fkey    FK CONSTRAINT     r   ALTER TABLE ONLY recurso
    ADD CONSTRAINT recurso_pedido_id_fkey FOREIGN KEY (pedido_id) REFERENCES pedido(id);
 H   ALTER TABLE ONLY public.recurso DROP CONSTRAINT recurso_pedido_id_fkey;
       public       renan    false    2137    190    204            �      x������ � �      �      x������ � �      �   "   x�3����K/M��2�,J�K�+N+����� n��      �   K   x��A� �3}�f�����(g����ʚ���uޏyq*!�w�&�5D�@wD�Abu`gfZe;`f?(//      
	   �  x��U�n7>SO1z���$Ǿ	�$��U�ч�vG2.�&w����C� �!h���>�ޤO��Y�/E� ���}�7�LMJ�=�������N-�Ѕ�%mu�^Z+^S�J��첗{���u�T��;}5�Ɋ�Zx�E�����늺@%x
�5Bh��Ο����K,]�����}��Py�� z��]�
��rbO��ӆ<�q��=���,M�a2f�E��'Yr5J���*fi��)���o0'��FGO��y��+A��A[�y<������׮��C�\[�%����4���YZr�Ֆc'�"$�֥��D�w��'��`�k�$��M�=̜��fGAB�Qh���!����%�݃Ų�z��Fs�]4y�K�)gC�H���j�-##�\,F���b�4�Gã�V>as���DO��Ao&�tȕ.)
���Б�)yv��g�$�jɺ�;�˱١wo���|� K,JP�IL;k;j@�R�����Y�g�/��8�m����^�o�I�(�������%�I����MѦ�"vG2Fg�~ ��R�<��V'��90
�>F:���/g�rOLw�$T�k4�C����D��N��L�0%"�k���`ZQ����G56�� Sܺ��p�X����ǜߪ�ib�2.DOg��a�e�ׁ��E����/���?��h[�E�.�yLr}Z�Ա/��#��Bp��ٜwW�y���Շ�g<��I� qK��w��#3���=����MԸDQ�4�+�����W������c[O�Q>;[�y��!]t�`�N�9�.��ȳ�U����ŋ4����<{k60�x�~����'��>�U��2��C[�d��ߢk�ۀ;2P6�k���e֐ݾ(��Ǉ���|�`�N      �   0  x�}W�r�6]_�h"���d)��1c�({�D�2fH�H-��ot�z�Ig��t�-��_�R����ʲ^�{Ϲ�^^%w�Mx�yɴ`4��HE�2���^D�U���V3C^��#e�]�P�=:;�?�ϙ&CD��:"o4۲�I!�,/4�r�wJ�+����c���N��0�����&,}R\ha_��%��~�J,��EјU����AC�ޘ���OW:�]���+�&���L�<Rш!{~�aߙ+��2�x���OT^0���m0���������<�=:	Z�L�Mn���:<CT��R�5��^��������#.�[�e�I�-�VH^r� R��x)���j��%��:�  5�Us��W�|%1��+�k��'�J�~���i����%2(����7�n���8?�JH�{q�V�f�^o�d����`f
�����F���u�Z��0a�盧��}ų7R}'�1�<;
��eG��T�@f�M��i\�f"=��=��uU2Y�/�w��P٤{��E%��@|'�s͑�A�z��#�����'W�&����$ �1����ߩ�]���k��e�R���
�^ko�g��J�0K�Ss���S�?曪���6�ZIu8Jj�T�����t|��g���.*;+�z��"��pֈKq�	:ʹ+9,5p�k� /T#hd�آ�F���$M�4�po�3�dpeIB/�?����y�y0 ��_�mo���}z�q��,b2�_�X��9�u�S�m"+��t~"���^�zl��IL��TtU����9��#���À*�-��`Z��8yZT�F30������� ��Z&`�VǍ�� $F�eW5�B[�6tY�L6�Ac؈��жg�v���o	��`tCp��.�C1[;<���e;��\Z�.�'��݀Z}�m4�y����Nˍ���wz�lW��풳� �঴#c�`��
�s���B@�3�������]�����M�Ïޤ��DI����E>�u��t�W�^
�߼�]��[� ���UG�g3m�ь���-�]��zY3V�EQ}���R)P���]��?�r��Z��S��k����g��B��MB��b��ަJ+]	�����
�MB��k���U֛×�aX]���
1;Rai����ѕ7�[��/�Iġ�Be�UE�;�Ȳ{�Q��
��CF���V�����.@Ӹ�TPh�<M�fcI9bᷴ&��E�=��X�O����M�"��ua�dZߚM;�sc��ʁ�߷*!����`U���7�6�!FN����@��=!�?Q��~      �   *   x�3�4204�50�54Q00�22�2��372�0������ mn      �   �  x��T=o�0��_q�l;�b[�7#qN�h+�����B"���![Jѡ�\t�?֣��-�m�
�q�{��#� ���(�'�2�ĸ'�kb&�\+��z��� �{�Q/�`0i�`�+P�rzCL�"@[�������Uj�d6��C�?Vh�����ݨ�/�gӅ�0/�u�[�`��i���,�W��y�t�����RHIM�7��Y�f�V%S�� # ��ѩ�P�Z��T��`u,Y�Q���{�*#�jS�:s5�^Kc���%G'ji�J�"��!q8	��;�����L ��bR<3pc
i_���`��y�l���ix��s�2t�*C�*�B���5��?$j�-Q�mN�[�Os�a$[��@�b�Z�b��L�$�K����S�ܪC��L˗�i�ݼ�����|���8�.�`N��%A�q�d��]�P��<w�ܵ=�������\.�{[풛���P�T�����KX8-�J�;� ǯ�����
�V'��8��\K�O����byo��0j�*9���+i�=#>��Yx�O0��1���}0�|孇�dK�m��g��uy�-�6���������zjҊ%TF�=�q/��$�D�$�/���(�Er���[шkq���j�Eg��t:? O�>      	      x������ � �      	      x�3�4�2bc 6�4����� j�      	      x�3�4�2bc 6�4����� j�      �      x������ � �      	   �  x�푿N�0���)N�[c;N�dC�D�XX�ԭ�J\�%}Ā�k^��ZFT�Z������HG�uu�\����-\w����n`�Xǝ�����Z���ʡkZ6��R`�-6�0�Lr��ܬ��nl�ֿ���-��Bc᮳P�MCu6��O-�v�#��3m�T���)@���aHt��/;���FU��"����~0N�����f�y���Q�+��^��#^ �D��γuC[��ֹI&�QBI�RaR��B&����")fY=���g�!?��l�HU�TF�,�<z�G2�'���IkZ��2Ѣ0�ɓ=6s�v,l��m���m����L���LEf�F�'|Ƈ������D*��o�OKf'e�8��W���e��      	      x�E˱
�0��9y��{�RK`]�xÁm�%�+� }1�A��ߛ��>t�<AU��R�lZ��a�0 �雹�\��F�`�pZ�d�?z�?m�Gp9)WR!�Y�,��ܑu���0�w�����Z��x(      	      x������ � �       	      x������ � �     