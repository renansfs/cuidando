import config from 'config'
import ajax from '../utils/ajax.js'
import MapStore from './mapStore'
import auth from './auth'
import {registerSignals} from '../utils/helpers'
import msgs from './msgs'

var api = config.apiurl_esic


function recursosCompare(a, b) {
    return a.history[0].date < b.history[0].date ? 1 : -1
}


class Recursos extends MapStore {
    constructor(signal) {
        super(signal)
        registerSignals(this, 'sendRecurso', true)
    }

    ajaxParams(key) {
        let url = `${api}/keywords/${key}`,
            method = 'get'
        return {url, method}
    }

    processResponse(json) {
        // Substitute strings for Dates
        for (let recurso of json.recursos) {
            recurso.request_date = new Date(recurso.request_date)
            for (let message of recurso.history) {
                message.date = new Date(message.date)
            }
        }
        return json.recursos.sort(recursosCompare)
    }

    // updatePedido(json) {
    //     if (json) {
    //         console.log('map', this._map, 'json', json)
    //         let key = json.keyword
    //         this._map[key] = this.processResponse(json)
    //         this.triggerChanged(key)
    //         console.log('UPDATE', key, json)
    //     }
    // }

    // Send a new recurso
    async sendRecurso(params) {
        let url = `${api}/recursos`,
            data = {
                'token': await auth.getMicroToken(),
                'text': params.text,
                'orgao': params.orgao,
                'keywords': params.protocolo,
            },
            ret = null
            //console.log(data)
        // this.updatePedido(await ajax({url, data, method: 'post'}))
        try {
            ret = await ajax({url, data, method: 'post'})
            if (ret) {
                // Force pedidos reload for this despesa
                this.load(params.keywords, true)
                msgs.addSuccess('Question sent')
            }
        } catch(err) {
            msgs.addError('error_send_question')
        }
        return ret
    }
}

let recursos = new Recursos('recursos')

export default recursos
