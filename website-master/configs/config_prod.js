const config = {
    apiurl_money: 'https://ga-muitosdados.rhcloud.com/api/v1/',
    // apiurl_money: 'https://site-cuidando.rhcloud.com/dados/api/v1/',
    apiurl_auth: 'https://viralata-cuidando.rhcloud.com',
    apiurl_comments: 'https://tagarela-cuidando.rhcloud.com',
    apiurl_esic: 'http://127.0.0.1:5004',
}

export default config
