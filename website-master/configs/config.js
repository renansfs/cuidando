const _domain = 'http://cuidando.aqui:'
const config = {
    // apiurl_money: 'https://site-cuidando.rhcloud.com/dados/api/v1/',
    // apiurl_money: _domain + 5000,
    // apiurl_money: 'http://localhost:5000/api/v1',
    apiurl_money: 'https://ga-muitosdados.rhcloud.com/api/v1/',
    apiurl_auth: 'https://viralata-cuidando.rhcloud.com',
    // apiurl_auth: _domain + 5002,
    apiurl_comments: 'https://tagarela-cuidando.rhcloud.com',
    // apiurl_comments: _domain + 5003,
    apiurl_esic: 'http://127.0.0.1:5004',
    // apiurl_esic: _domain + 5004,
}

export default config
